﻿var url_get_profile     = url_base + "Profile";
var url_save_profile    = url_base + "Profile";
var url_change_condo    = url_base + "auth/login";
var url_change_avatar   = url_base + "Avatar";

var PerfilComponent = {
    controller: function () {
        var ctrl = this;

        ctrl.profile = {
            condo: localStorage.getItem('condo'),
            name: localStorage.getItem('name'),
            home: localStorage.getItem('home'),
            lang: localStorage.getItem('lang'),
            condo_id: localStorage.getItem('condo_id'),
        }

        this.condo      = m.prop(ctrl.profile.condo || null);
        this.name       = m.prop(ctrl.profile.name || null);
        this.home       = m.prop(ctrl.profile.home || null);
        this.lang       = m.prop(ctrl.profile.lang || null);
        this.condo_id   = m.prop(ctrl.profile.condo_id || null);
        this.file       = m.prop('images/avatar.svg');

        ctrl.condos     = {};
        ctrl.languages  = {};
        ctrl.error      = {};
        ctrl.update     = false;
        
        ctrl.change = function (e) {
            e.preventDefault();
            var r = confirm("Confirma que deseas cambiar de condominio");
            if (r == true) {
                m.request({
                    method: 'POST',
                    url: url_change_condo,
                    headers: {"APPTOKEN":localStorage.getItem('header') },
                    config: function (xhr) {
                        xhr.timeout = 10000;
                        xhr.ontimeout = function () {
                            alert('Parece que la conexión es un poco lenta.');
                        }
                    },
                    data: {token:localStorage.getItem('token'), condo: ctrl.condo_id},
                    unwrapSuccess: function (response) {
                        if (response.status == "OK" && response.data.token) {
                            localStorage.setItem("token", response.data.token);
                            localStorage.setItem("lang", response.data.lang);
                            localStorage.setItem("currency", response.data.currency);
                            localStorage.setItem("name", response.data.name);
                            localStorage.setItem("condo", response.data.condo);
                            localStorage.setItem("condo_id", response.data.condo_id);
                            window.location = 'index.html';
                        }
                    }
                })
            } else 
                return false;
        }

        ctrl.save = function (e) {
            e.preventDefault();
            m.request({
                method: 'POST',
                url: url_save_profile,
                headers: { "APPTOKEN": localStorage.getItem('header') },
                config: function (xhr) {
                    xhr.timeout = 10000;
                    xhr.ontimeout = function () {
                        alert('Parece que la conexión es un poco lenta.');
                    }
                },
                data: ctrl.profile,
                unwrapSuccess: function (response) {
                    if (response.status == "OK" && response.data.token) {
                        localStorage.setItem("token", response.data.token);
                        localStorage.setItem("lang", response.data.lang);
                        localStorage.setItem("currency", response.data.currency);
                        localStorage.setItem("name", response.data.name);
                        localStorage.setItem("condo", response.data.condo);
                        localStorage.setItem("condo_id", response.data.condo_id);
                       
                        window.location = 'index.html';
                    }

                    if (response.status == "OK" && !response.data.token)
                        ctrl.update = true;

                    if (response.status == 'ERROR') 
                        ctrl.error = response.messages;
                },
                unwrapError: function (response) {
                    ctrl.error = response.messages;
                }
            })
            .then(m.redraw)
        }

        var profile = m.request({
            method: "GET",
            url: url_get_profile,
            headers: { "APPTOKEN": localStorage.getItem('header') },
            config: function (xhr) {
                xhr.timeout = 10000;
                xhr.ontimeout = function () {
                    alert('Parece que la conexión es un poco lenta.');
                }
            },
            background: true,
            initialValue: []
        })

        ctrl.B64File = function (prop){
            
            return function (ev) {
                
                var file = ev.target.files[0];
                
                if (!file) return;

                var reader = new FileReader();

                reader.onload = function (readerEvt) {
                    var binaryString = readerEvt.target.result; 
                    var data = new FormData();

                    file_send = btoa(binaryString);
                    
                    data.append("file1", file_send)
                    data.append("filename1", file.name)

                    m.request({
                        method: "POST",
                        url: url_change_avatar,
                        headers: { "APPTOKEN": localStorage.getItem('header') },
                        config: function (xhr) {
                            xhr.timeout = 10000;
                            xhr.ontimeout = function () {
                                alert('Parece que la conexión es un poco lenta.');
                            }
                        },
                        data: data,
                        serialize: function (data) { return data },
                        unwrapSuccess: function (response) {
                            if (response.status == 'OK') {
                                ctrl.file = m.prop(response.data.avatar+response.data.nomavatar)
                                ctrl.update = true;
                                localStorage.setItem("avatar", ctrl.file());
                            } else {
                                ctrl.error = response.messages;
                            }

                        },
                        unwrapError: function (response) {
                            ctrl.error = { server: "Error de conexión con el servidor" };
                        }
                    })
                    .then(m.redraw)
                    m.redraw();
                }

                reader.readAsDataURL(file);
            }
        }

        profile.then(function (response) {
            ctrl.profile.condo_id   =  localStorage.getItem("condo_id"),
            ctrl.profile.name       = response.data.name;
            ctrl.profile.condo      = response.data.condo;
            ctrl.profile.lang       = response.data.language;
            ctrl.condos             = response.data.condos;
            ctrl.languages          = response.data.languages;
            ctrl.file               = m.prop(response.data.avatar + response.data.nomavatar);
        })
        profile.then(m.redraw)
        localStorage.inSection = 'profile';
    },
    view: function (ctrl) {
        if (ctrl.condos.length === undefined)
            return Working;

        return m("div.profile-content", [
            m("div.profile-image", [
                m("img#avatar_profile_change", {src:ctrl.file()}),
                m("div.upload", m("input[type='file']", { oninput: m.withAttr("value", ctrl.file1), class: 'form-control', style: "border-bottom:0px !important", name: 'file[]', onchange: ctrl.B64File(ctrl.file1) })),
                m("p.name",m("span.name", localStorage.getItem("name"))),
                m("p.tower", m("span.tower", localStorage.getItem("vivienda")))
            ]),
            m("div.col-xs-12", [
                (ctrl.update) ? m("div.alert alert-success", 'Tu perfil se actualizo con exito'):'',  
                m("div.profile-input[style='text-align:center']", [
                    m("label.profile_label[tkey='profile-label-house']", "Condominio: "),
                    m('h4.tower', ctrl.condo())
                    //m("input", { type: 'text', class: 'form-control', value: ctrl.condo() })
                ]),
                /*(ctrl.condos.length > 1
                ?
                m("div.profile-input", [
                    m("label.profile_label[tkey='profile-label-condo']", "Cambiar de condominio: "),
                    m("select.form-control#profile_condos[name='profile_condos']", { oninput: m.withAttr("value", ctrl.condo_id), value: ctrl.condo_id(), onchange: ctrl.change }, [ctrl.condos.map(function (condo) {
                        return m("option", { value: condo.User }, condo.Condo);
                    })])
                ])
                :
                ""
                ),*/
                /*
                m("div.profile-input", [
                    m("label.profile_label[tkey='profile-label-language']", "Lenguaje: "),
                    m("select.form-control#profile_language[name='profile_language']", { oninput: m.withAttr("value", ctrl.lang), value: ctrl.lang() }, [
                        ctrl.languages.map(function (lang) {
                            return m("option", { value: lang.code }, lang.name)
                        })
                    ])
                ])*/
            ])
        ])
    }
}