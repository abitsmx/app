﻿var url_notifications = url_base + "Notificaciones";
var Notificaciones = {
    controller: function () {
        var ctrl = this;
        ctrl.notifications = [];
        ctrl.errors = [];

        var notifications = m.request({
            method: "GET",
            url: url_notifications,
            headers: { "APPTOKEN": localStorage.getItem('header') },
            config: function (xhr) {
                xhr.timeout = 10000;
                xhr.ontimeout = function () {
                    alert('Parece que la conexión es un poco lenta.');
                }
            },
            background: true,
            initialValue: [],
            unwrapSuccess: function (response) {
                if (response.status == "OK") {
                    ctrl.notifications = response.data;
                }

                if (response.status == 'EXIST') {
                    localStorage.clear();
                    localStorage.setItem("exist", true);
                    window.location = 'login.html';
                }

                if (response.status == 'ERROR') {
                    ctrl.errors = response.messages;
                }
            },
        })
        notifications.then(m.redraw)
        localStorage.inSection = 'notifications';
    },
    view: function (ctrl) {
        if (ctrl.errors.length > 0) {
            return m("div", [
                ctrl.errors.map(function (error) {
                    return m("span.alert alert-danger", error);
                })
            ]);
        }

        if (ctrl.notifications.length == 0)
            return Working;
        
        return m("div", [
            ctrl.notifications.map(function (notify) {
                return m("div.content-notify", [m("div.image", m("img", { src: notify.avatar == "" ? 'images/avatar70x70.png' : notify.avatar + '70' + notify.nomavatar, config:m.route })),
                        m("div.data-notify", [
                            m("span.notify-creo", notify.Creo+' '),
                            m("label", ' hizo un comentario en '),
                            m("a", { href: "/blog/" + notify.BlogId, config: m.route }, ' '+notify.Asunto),
                            m("label.tower-notify", notify.vivienda),
                            m("label.date-notify", notify.CmmtCreatedDate)
                        ]),
                        ])
            })
        ]);
    }
};