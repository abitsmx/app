﻿var url_new_post = url_base + "Posts/";
var numFile = 1;

var PostComponent = {
    controller: function () {
        var ctrl = this;

        ctrl.post = {
            title: '',
            content: ''
        }

        
        this.title      = m.prop(ctrl.post.title || '');
        this.content    = m.prop(ctrl.post.content || '');
        this.file1      = m.prop(null);
        this.file2      = m.prop(null);
        this.file3      = m.prop(null);
        
        ctrl.nameFile1  = m.prop(null);
        ctrl.nameFile2  = m.prop(null);
        ctrl.nameFile3  = m.prop(null);

        ctrl.error = [];
        ctrl.success = false;

        ctrl.newPost = function (e) {
            e.preventDefault();
            
            var data = new FormData();

            data.append("title", ctrl.title());

            if (ctrl.file1()) {
                data.append("file1", ctrl.file1())
                data.append("filename1", ctrl.nameFile1())
            }

            if (ctrl.file2()) {
                data.append("file2", ctrl.file2())
                data.append("filename2", ctrl.nameFile2())
            }

            if (ctrl.file3()) {
                data.append("file3", ctrl.file3())
                data.append("filename3", ctrl.nameFile3())
            }

            data.append("content", ctrl.content());
            
            m.request({
                method: 'POST',
                url: url_new_post + m.route.param("section_id"),
                data: data,
                serialize: function(data) {return data},
                unwrapSuccess: function (response) {
                    if (response.status == 'OK') {

                        ctrl.title      = m.prop('');
                        ctrl.content    = m.prop();
                        ctrl.file1      = m.prop();
                        ctrl.file2      = m.prop();
                        ctrl.file3      = m.prop();
                        ctrl.nameFile1  = m.prop();
                        ctrl.nameFile2  = m.prop();
                        ctrl.nameFile3  = m.prop();
                        numFile = 1;
                        ctrl.success = true;
                    } else {
                            ctrl.error = response;
                    }

                },
                unwrapError: function (response) {
                    ctrl.error = {server:"Error de conexión con el servidor"};
                }
            })
            .then(m.redraw)
        }

        ctrl.resetF1 = function () {
            ctrl.file1 = m.prop();
            ctrl.file2 = m.prop();
            ctrl.file3 = m.prop();
            m.redraw()
        }

        ctrl.resetF2 = function () {
            ctrl.file2 = m.prop();
            ctrl.file3 = m.prop();
            m.redraw()
        }

        ctrl.resetF3 = function () {
            ctrl.file3 = m.prop();
            m.redraw()
        }

        ctrl.canSubmit = function () {
            return ctrl.title() && ctrl.content();
        }

        ctrl.B64File = function (prop){
            
            return function (ev) {
                
                var file = ev.target.files[0];
                
                if (!file) return;

                var reader = new FileReader();

                reader.onload = function (readerEvt) {
                    var binaryString = readerEvt.target.result; 
                    
                    if (numFile == 1) {
                        ctrl.nameFile1 = m.prop(file.name);
                    }

                    if (numFile == 2)
                        ctrl.nameFile2 = m.prop(file.name);

                    if(numFile == 3)
                        ctrl.nameFile3 = m.prop(file.name);
                    
                    prop(btoa(binaryString));
                    numFile++;
                    m.redraw();
                }

                reader.readAsDataURL(file);
            }
        }
        
        ctrl.section_id = m.route.param("section_id");
        localStorage.inSection = 'blog';
    },
    view: function (ctrl) {
        return m("div.col-md-12", m("form#new_post", [
                    m("div.title-sections", m("h3", "Nuevo post")),
                    (ctrl.success ? m("div.alert alert-success", "Post agregado con exito") : ''),
                    (ctrl.error.length > 0 ? m("div.alert alert-warning", [ctrl.error.map(function (err, i) { return m('label',err)})]) : ''),
                    m("div.form-group", [
                       m("label", "Titulo: "),
                       m("input", { oninput: m.withAttr("value", ctrl.title), class: 'form-control', placeholder: "Título de tu post", type: "text", value: ctrl.title(), name: "title" })
                    ]),
                    m("div.form-group", [
                       m("label", "Contenido: "),
                    m("textarea.form-group", { oninput: m.withAttr("value", ctrl.content), class: 'form-control', placeholder: "Agrega contenido del blog", name: 'content' }, ctrl.content()),
                    ]),
                    ctrl.file1()
                    ?
                        m("div.content-img", [
                            m("img", { src: atob(ctrl.file1()), width: '100px' }),
                            m("a.glyphicon glyphicon-remove-sign", {onclick:ctrl.resetF1})
                        ])
                    :
                        m("div.form-group.formFile", [
                            m("label", "Adjutar imagen o archivo: "),
                            m("div.uploads", m("input[type='file']", { oninput: m.withAttr("value", ctrl.file1), class: 'form-control', style: "border-bottom:0px !important", name: 'file[]', onchange: ctrl.B64File(ctrl.file1) }))
                        ]),
                        ctrl.file1()
                        ?
                        (
                        ctrl.file2()
                        ?
                        m("div.content-img", [

                            m("img", { src: atob(ctrl.file2()), width: '100px' }),

                            m("span.glyphicon glyphicon-remove-sign", {onclick:ctrl.resetF2})
                        ])
                        :
                        m("div.form-group.formFile", [
                            m("label", "Adjutar imagen o archivo: "),
                            m("div.uploads", m("input[type='file']", { oninput: m.withAttr("value", ctrl.file2), class: 'form-control', style: "border-bottom:0px !important", name: 'file[]', onchange: ctrl.B64File(ctrl.file2) }))
                        ])
                        )
                    : null,
                    ctrl.file2() ?
                        (
                        ctrl.file3()
                        ?
                        m("div.content-img", [
                            m("img", { src: atob(ctrl.file3()), width: '100px' }),
                            m("span.glyphicon glyphicon-remove-sign", { onclick: ctrl.resetF3 })
                        ])
                        :
                        m("div.form-group.formFile", [
                            m("label", "Adjutar imagen o archivo: "),
                            m("div.uploads", m("input[type='file']", { oninput: m.withAttr("value", ctrl.file3), class: 'form-control', style: "border-bottom:0px !important", name: 'file[]', onchange: ctrl.B64File(ctrl.file3) }))
                        ])
                        )
                    :null,
                    m("div.form-group", [   
                    m("button", { onclick: ctrl.newPost, class: 'btn btn-default btn-block', disabled: !ctrl.canSubmit() }, m("span.glyphicon glyphicon-send", " Enviar")),
                    ]),
                    m("div.volver", m("a[href='/section/" + ctrl.section_id + "'][class='resalta']", { config: m.route }))
                ])
            );
    }
};