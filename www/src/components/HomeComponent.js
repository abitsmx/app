﻿var HomeComponent = {
    view: function () {
        return m("div.content-home", [
                    m("div.content-menu-logo", [
                        m("div.content-menu", [
                            m("div.content-menu-img menu-perfil", [m("a[href='/perfil']", { config: m.route }, m("span.glyphicon glyphicon-user")), m("label.name-menu", "Perfil")]),
                    ]),
                            m("div.menu-description", [
                                    m("a.link-perfil[href='/perfil']", { config: m.route }, "Modifica tus datos personales")
                            ])
                        ]),
                    m("div.content-menu-logo", [
                        m("div.content-menu", [
                            m("div.content-menu-img menu-blog", [m("a[href='/blogs']", { config: m.route }, m("span.glyphicon glyphicon-list-alt")), m("label.name-menu", "Blogs")]),
                     ]),
                      m("div.menu-description", [
                                    m("a.link-blog[href='/blogs']", { config: m.route }, "Comunicados, Crea y comenta.")
                      ])
                        ]),
                      m("div.content-menu-logo", [
                        m("div.content-menu", [
                            m("div.content-menu-img menu-cuenta", [m("a[href='/cuenta']", { config: m.route }, m("span.glyphicon glyphicon-indent-right")), m("label.name-menu", "Estado de cuenta")]),
                        ]),
                      
                            m("div.menu-description", [
                                    m("a.link-cuenta[href='/cuenta']", { config: m.route }, "Adeudos pendientes, Pagos y Saldos")
                            ])
                        ])
        ])
    }
};