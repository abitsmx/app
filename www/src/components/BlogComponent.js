﻿var url_blog = url_base+"Posts";
var BlogComponent = {};

BlogComponent.controller = function () {
    var ctrl = this;
    ctrl.blogs = [];
    var working = true;
    var blogs = m.request({
        method: "GET",
        url: url_blog,
        background: true,
        initialValue: [],
                    unwrapSuccess: function (response) {
                    if (response.status == "OK") {
                       ctrl.blogs = response.data;
                    }

                    if (response.status == 'EXIST') {
                        localStorage.clear();
                        localStorage.setItem("exist", true);
                        window.location = 'login.html';
                    }

                    if (response.status == 'ERROR') {
                        ctrl.error = response.messages;
                    }

                    working = false;
                }
        })

        localStorage.inSection = 'home';
        blogs.then(m.redraw)
};

BlogComponent.view = function (ctrl) {

    if (ctrl.working && ctrl.blogs.length == 0)
        return Working;
   
    if (ctrl.blogs.length == 0)
            return m("div.alert alert-info", "No hay registro de Post. Ingresa a alguna sección para agregar post.");

    return m("div", [
        ctrl.blogs.map(function (blog) {
            return m("div.div_base.resulset-content col-xs-12", [
                m("div.photo col-xs-3",m('img', {src:(blog.avatar == '' ? 'images/avatar70x70.png' : blog.avatar+'40'+blog.nomavatar)})),
                m("div.col-xs-9", [
                    m("h3.blog-title", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, blog.title),
                    ]),
                    m("div.authoring", [
                        m("p", [
                            blog.author + ' ' + blog.date,
                            m("a.blog-section[href='/section/" + blog.section_id+ "']",  { config: m.route }, blog.section)
                        ])
                    ]),
                ]),
                m("div.post-content", [m("p", blog.sumary)]),
                m("div.labels", [
                    m("span.add_comment", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, [
                            m("span.badge", [
                                m("span.glyphicon glyphicon-comment", ""),
                                ' ' + blog.comments,
                            ] )                         
                        ])
                    ])
                ]),
                m("div.labels", [
                    m("span.comments-count", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, [
                            m("span.badge", [
                                m("span.glyphicon glyphicon-paperclip", ""),
                                m("span[tkey='blog-label-view']", ""),
                                ' ' + blog.files,
                            ])
                        ])
                    ])
                ]),
                m("div.labels", [
                    m("span.comments-count", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, [
                            m("span.badge", [
                                m("span.glyphicon glyphicon-camera", ""),
                                m("span[tkey='blog-label-view']", ""),
                                ' ' + blog.images,
                            ])
                        ])
                    ])
                ]),
                m("div.labels",[
                    m("span.comments-count", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, [
                            m("span.badge",[
                                m("span.glyphicon glyphicon-eye-open", ""),
                                m("span[tkey='blog-label-view']", "")
                            ])
                        ])
                    ])
                ])
            ])
        })
    ])
};