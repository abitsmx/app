﻿    var url_reset = url_base + "reset.php";
    var ResetForm = {
        controller: function () {
            var ctrl = this;

            ctrl.email = '';
            ctrl.success = false;

            this.email = m.prop('');

            ctrl.canSubmit = function () {
                return ctrl.email();
            }

            ctrl.reset = function (e) {
                e.preventDefault();
                m.request({
                    method: 'POST',
                    url: url_reset,
                    headers: { "APPTOKEN": localStorage.getItem('header') },
                    config: function (xhr) {
                        xhr.timeout = 10000;
                        xhr.ontimeout = function () {
                            alert('Parece que la conexión es un poco lenta.');
                        }
                    },
                    data: { 'username': ctrl.email },
                    unwrapSuccess: function (response) {
                        if (response.status == 'OK') {
                            ctrl.success = true;
                            ctrl.email = m.prop('');
                            }

                        if (response.status == "ERROR") {
                            localStorage.clear();
                            window.location = 'login.html';
                        }
                    }
                    
                })
                .then(m.redraw);
            }
        },
        view: function (ctrl) {
            return m("div.col-sm-12", [
                    m("form#form_reset", [
                        m("div.form-group", [
                            (ctrl.success === false ? m("div.alert alert-info", "Favor de ingresar correo electrónico para recuperar password.") : m("div.alert alert-success", "Se le ha enviado su password al correo electrónico.")),
                            m("label", "Email: "),
                            m("input.form-control[placeholder='usuarios@vivook.com'][name='username']", {oninput:m.withAttr("value", ctrl.email), value: ctrl.email()}),
                        ]),
                        m("div.form-group", [
                            m("button.btn btn-default btn-block[type=button]", { onclick: ctrl.reset, disabled: !ctrl.canSubmit() }, "Enviar")
                        ])
                    ]),
                    m("div.volver", m("a[href='/Login']", { config: m.route }))
                ])
        }
    };