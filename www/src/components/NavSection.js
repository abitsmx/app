﻿var url_sections = url_base + "Secciones";
var NavSection = {

    controller: function () {
        section_id = m.route.param("section_id") || '';
        localStorage.setItem('section_active', section_id);

        var ctrl = this;
        ctrl.sections = [];

        var sections = m.request({
            method: "GET",
            url: url_sections,
            headers: { "APPTOKEN": localStorage.getItem('header') },
            config: function (xhr) {
                xhr.timeout = 10000;
                xhr.ontimeout = function () {
                    alert('Parece que la conexión es un poco lenta.');
                }
            },
            background: true,
            initialValue: [],
                unwrapSuccess: function (response) {
                    if (response.status == "OK") {
                       ctrl.sections = response.data;
                    }
                    
                    if (response.status == 'EXIST') {
                        localStorage.clear();
                        localStorage.setItem("exist", true);
                        window.location = 'login.html';
                    }

                    if (response.status == 'ERROR') {
                        ctrl.error = response.messages;
                        console.log(response.messages[0]);
                    }
                },
        })
        localStorage.inSection = 'blog';
        sections.then(m.redraw)
    },
    view: function (ctrl) {
        if( ctrl.sections.length ==  0)
            return Working;

        if (localStorage.getItem('section_active')) {
            title = ctrl.sections.map(function (section) {
                if( localStorage.getItem('section_active') == section.section_id)
                    return section.section
            })
        } else
            title = "Secciones del Blog";

        return m("div", [
            m("div.title-sections", m("h3", title)),
            m("div.content-list", [
                m("div.nav navbar-nav", [
                        m("div.active", m("a[href='/']", { config: m.route }, 'Todas')),
                        ctrl.sections.map(function (section) {
                            return m("div.active", [
                                m("a[href='/section/" + section.section_id + "']", { config: m.route }, m.trust(section.section)),
                            ])
                        }),
                ]),
            ]),
            m("div.separator-footer")
        ])
    }
};