﻿var url_section = url_base + "Posts/"
var Sections = {};

Sections.controller = function () {
    var ctrl = this;

    ctrl.blogs = [];
    ctrl.section_id = m.route.param("section_id");
    ctrl.post = '';
    ctrl.working = true;

    var blogs = m.request({ 
                        method: "GET", 
                        url: url_section + m.route.param("section_id"),
                        headers: { "APPTOKEN": localStorage.getItem('header') },
                        config: function (xhr) {
                            xhr.timeout = 10000;
                            xhr.ontimeout = function () {
                                alert('Parece que la conexión es un poco lenta.');
                            }
                        },
                        background: true, 
                        initialValue: [],
                        unwrapSuccess: function (response) {
                                    if (response.status == "OK") {
                                        ctrl.blogs = response.data;
                                        ctrl.post = response.post;
                                        ctrl.working = false;
                                    }

                                    if (response.status == 'fail') {
                                      
                                        ctrl.error = [response.message];
                                    }
                        }
                })
    blogs.then(m.redraw)
    localStorage.inSection = 'blog';
};

Sections.view = function (ctrl) {

    if (ctrl.working)
        return Working

    if (ctrl.blogs.length == 0)
        return m("div", [
            (ctrl.post
                ?
            m("div.content-section", m("a.btn btn-warning[href='/newPost/" + ctrl.section_id + "']", {config:m.route}, [
                    m("span.glyphicon glyphicon-send", " Nuevo post"),
                ])
            )
            :
            m("div.alert alert-info", "No tiene permisos para crear post en esta sección")
            ),
            m("div.alert alert-warning", [
            m("strong", "Advertencia: "),
            "No hay resultados de post para la sección"
            ]),
            m("div.volver", m("a[href='/blogs']", { config: m.route }))
            ]);

    return m("div", [
        ( ctrl.post
            ?
        m("div.content-section", m("a.btn btn-warning[href='/newPost/" + ctrl.section_id + "']", { config: m.route }, [
            m("span.glyphicon glyphicon-send", " Nuevo post"),
            ])
        )
        :
        null
        ),
        ctrl.blogs.map(function (blog) {
            return m("div.div_base.resulset-content col-xs-12", [
                m("div.photo col-xs-3", m('img', { src: (blog.avatar == '' ? 'images/avatar70x70.png' : blog.avatar+'70'+blog.nomavatar) })),
                m("div.col-xs-9", [
                    m("h3.blog-title", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, blog.title),
                    ]),
                    m("div.authoring", [
                        m("p", [
                            blog.author + ' ' + blog.date,
                            m("a.blog-section[href='/section/" + blog.section_id + "']", { config: m.route }, blog.section)
                        ])
                    ]),
                ]),
                m("div.post-content", [m("p", blog.sumary)]),
                 m("div.labels", [
                    m("span.add_comment", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, [
                            m("span.badge", [
                                m("span.glyphicon glyphicon-comment", ""),
                                ' ' + blog.comments,
                            ])
                        ])
                    ])
                 ]),
                m("div.labels", [
                    m("span.comments-count", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, [
                            m("span.badge", [
                                m("span.glyphicon glyphicon-paperclip", ""),
                                m("span[tkey='blog-label-view']", ""),
                                ' ' + blog.files,
                            ])
                        ])
                    ])
                ]),
                m("div.labels", [
                    m("span.comments-count", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, [
                            m("span.badge", [
                                m("span.glyphicon glyphicon-camera", ""),
                                m("span[tkey='blog-label-view']", ""),
                                ' ' + blog.images,
                            ])
                        ])
                    ])
                ]),
                m("div.labels", [
                    m("span.comments-count", [
                        m("a[href='/blog/" + blog.blog_id + "']", { config: m.route }, [
                            m("span.badge", [
                                m("span.glyphicon glyphicon-eye-open", ""),
                                m("span[tkey='blog-label-view']", "")
                            ])
                        ])
                    ])
                ])
            ])
        }),
        m("div.volver", m("a[href='/blogs']", { config: m.route }))
    ])
};
