﻿var url_login = url_base + "auth/login";

var Auth = {
    controller: function () {
        var ctrl = this;
  
        ctrl.user = {
            username: null,
            password: null,
            condo: null
        }
       
        this.username   = m.prop(ctrl.user.username || null);
        this.password   = m.prop( ctrl.user.password || null);
        this.condo      = m.prop(ctrl.user.condo || null);

        ctrl.error  = {};
        ctrl.condos = {};

        var log = function (value) {
            return value;
        }
        ctrl.canSubmit = function () {
            return ctrl.username() && ctrl.password();
        }

        ctrl.login = function (e) {
            e.preventDefault();
            if (ctrl.condo != null)
                ctrl.user.condo = ctrl.condo;
            
            m.request({
                method: 'POST',
                url: url_login,
                /*config: function (xhr) {
                    xhr.timeout = 10000;
                    xhr.ontimeout = function () {
                        alert('Parece que la conexión es un poco lenta.');
                    }
                },*/
                data: {user:ctrl.username(), pass:ctrl.password(), condo:ctrl.condo()},
                unwrapSuccess: function (response) {
                    if (response.status == "OK" && response.data.token) { localStorage.setItem("token", response.data.token);
                       
                        localStorage.setItem("lang", response.data.lang);
                        localStorage.setItem("currency", response.data.currency);
                        localStorage.setItem("name", response.data.name);
                        localStorage.setItem("home", response.data.condoname);
                        localStorage.setItem("condo", response.data.condoname);
                        localStorage.setItem("condo_id", response.data.userid);
                        localStorage.setItem("logo", response.data.logo);
                        localStorage.setItem("avatar", response.data.avatar);
                        localStorage.setItem("nomavatar", response.data.nomavatar);
                        localStorage.setItem("saldo", response.data.saldo);
                        localStorage.setItem("signo", response.data.signo);
                        localStorage.setItem("vivienda", response.data.vivienda);
                        localStorage.setItem("isAdmin", response.data.admin);
                        localStorage.setItem("header", response.data.header)
                        window.location = 'index.html';
                    }

                    if (response.status == "OK" && !response.data.token) {
                        ctrl.condos = response.data;
                    }

                    if (response.status == 'fail' || response.status == 'ERROR') {
                      
                        ctrl.error = response.messages;
                    }
                },
                unwrapError: function (response) {
                    ctrl.error = ["Error de conexion"];
                }
            })
        };

        if (localStorage.getItem('exist')) {
            ctrl.error = ["Has iniciado sesión en otro dispositivo."];
            localStorage.clear();
        } 
    },
    view: function (ctrl) {
        if (ctrl.condos.length) {
            return m("div", [
                m("div.alert alert-info", "Estas registrado en más de un condominio; selecciona el condominio al que deseas ingresar"),
                m("select.form-control select_condo_login", { oninput: m.withAttr("value", ctrl.condo), onchange: ctrl.login, value: ctrl.condo() }, m("option", { value: null }, "Selecciona condominio"), [ctrl.condos.map(function (condo) {
                    return m("option", { value: condo.User }, condo.Condo);
                })]),
                m("div.volver", m("a[href='/Login']", { config: m.route }))
            ]);
        } else {

            return m("div.col-sm-12", [
                       m("form#form_login", [
                           m("div.form-group", [
                               m("label", "Email: "),
                               m("input.form-control[type='email'][placeholder='usuarios@vivook.com']", {
                                   oninput: m.withAttr("value", ctrl.username), value: ctrl.username()
                               }),
                           ]),
                           m("div.form-group", [
                               m("label", "Password: "),
                               m("input.form-control[type='password'][placeholder='your password']", {
                                   oninput: m.withAttr("value", ctrl.password), value: ctrl.password()
                               }),
                           ]),
                           m("div.form-group", [
                               m("button.btn btn-default btn-block", { onclick: ctrl.login, disabled: !ctrl.canSubmit() }, "Entrar")
                           ])
                       ]),
                       (ctrl.error.length > 0) ? m('div.alert alert-warning', [
                            ctrl.error.map(function (error) {
                                return m("small", error)
                            })
                       ]) : '',
                       m("div.col-sm-12 reset-password", [m("a[href='/reset'][class='resalta']", { config: m.route }, "¿Olvidaste tu password?")])
            ])
        }
        }
};