﻿var url_new_help = url_base + "Sugerencia";

var HelpComponent = {
    controller: function () {
        var ctrl = this;

        ctrl.post = {
            type: '',
            section: '',
            comment:''
        }


        this.type = m.prop(ctrl.post.type || '');
        this.section = m.prop(ctrl.post.section || '');
        this.comment = m.prop(ctrl.post.comment || '');
        
        ctrl.error = [];
        ctrl.success = false;

        ctrl.newHelp = function (e) {
            e.preventDefault();

            var data = new FormData();

            data.append("tipo", ctrl.type());
            data.append("seccion", ctrl.section());
            data.append("descripcion", ctrl.comment());
            data.append("fuente", 'app');

            m.request({
                method: 'POST',
                url: url_new_help,
                headers: { "APPTOKEN": localStorage.getItem('header') },
                config: function (xhr) {
                    xhr.timeout = 10000;
                    xhr.ontimeout = function () {
                        alert('Parece que la conexión es un poco lenta.');
                    }
                },
                data: data,
                serialize: function (data) { return data },
                unwrapSuccess: function (response) {
                    if (response.status == 'OK') {
                        ctrl.type = m.prop('');
                        ctrl.section = m.prop();
                        ctrl.comment = m.prop();
                        ctrl.success = true;
                    } 
                    if (response.status == 'ERROR') {
                        ctrl.error = response.messages;
                    }
                },
                unwrapError: function (response) {
                    ctrl.error = { server: "Error de conexión con el servidor" };
                }
            })
            .then(m.redraw)
        }

        ctrl.canSubmit = function () {
            return ctrl.type() && ctrl.section() && ctrl.comment();
        }
        localStorage.inSection = '';
    },
    view: function (ctrl) {
        return m("div.col-md-12", m("form#new_help", [
                    m("div.title-sections", m("h3", "Reporte de mejoras")),
                    (ctrl.success ? m("div.alert alert-success", "¡Gracias! tendremos en cuenta tus comentarios") : ''),
                    (ctrl.error.length > 0 ? m("div.alert alert-warning", [ctrl.error.map(function (err, i) { return m('label', err) })]) : ''),
                    m("div.form-group", [
                       m("label", "Sección: "),
                       m("select.form-control", { oninput: m.withAttr("value", ctrl.section), value: ctrl.section() }, [
                            m("option", { value: 'blogs' }, "Blogs"),
                            m("option", { value: 'cuenta' }, "Estado de cuenta"),
                            m("option", { value: 'perfil' }, "Mi perfil")
                       ])
                    ]),
                    m("div.form-group", [
                       m("label", "Tipo: "),
                       m("select.form-control", { oninput: m.withAttr("value", ctrl.type), value: ctrl.type() }, [
                           m("option", { value: "M" }, "Mejora"),
                           m("option", { value: "E" }, "Error")
                       ])
                    ]),
                    m("div.form-group", [
                       m("label", "Comentario: "),
                    m("textarea.form-group", { oninput: m.withAttr("value", ctrl.comment), class: 'form-control', placeholder: "Ayudanos a mejorar..." }, ctrl.comment()),
                    ]),
                    m("div.form-group", [
                    m("button", { onclick: ctrl.newHelp, class: 'btn btn-default btn-block', disabled: !ctrl.canSubmit() }, m("span.glyphicon glyphicon-send", " Enviar")),
                    m("div.volver", m("a[href='/']", { config: m.route }))
                    ])
        ])
            );
    }
};