﻿var url_cuenta = url_base + "EstadoCuenta";
var url_viviendas = url_base + "Viviendas";

var CuentaComponent = {};

CuentaComponent.data = m.prop(null);

CuentaComponent.controller = function () {
    var ctrl = this;
    
    ctrl.data = {}
    ctrl.viviendas = {}
    ctrl.myApto = m.prop(null);
    localStorage.setItem('inApto', null);

    losDatos = function (e) {
        
        if (e > 0)
            url_cuenta = url_base + "EstadoCuenta/" + e;
        
        m.request({
            method: "GET",
            url: url_cuenta,
            unwrapSuccess: function (response) {
                if (response.status == "OK") {
                    ctrl.data = response.data;
                }

                if (response.status == 'ERROR') {

                    ctrl.error = [response.message];
                }
            }
        })
            .then(CuentaComponent.data);
    }

    if (ctrl.myApto() == null)
        losDatos('0', url_cuenta);
   

    m.request({
        method: "GET",
        url: url_viviendas,
        unwrapSuccess: function (response) {
            console.log(response);
            if (response.status == "OK") {
                ctrl.viviendas = response.data;
            }

            if (response.status == 'ERROR') {

                ctrl.error = [response.message];
            }
        }
    })
        .then(CuentaComponent.viviendas);

    ctrl.changeApto = function () {
        if (ctrl.myApto() > 0 && localStorage.getItem("inApto") != ctrl.myApto()) {
            localStorage.setItem('inApto', ctrl.myApto());
            losDatos(localStorage.getItem('inApto'));
        }
    }

    localStorage.inSection = 'statement';
}

CuentaComponent.view = function (ctrl) {
    
    var clase_saldo = (ctrl.data.signosaldofinal == 0 ? 'stablas' : (ctrl.data.signosaldofinal == '-' ? 'snegativo' : 'spositivo'));
    var mi_saldo = (ctrl.data.signosaldofinal == 0 ? 'Saldo final' : (ctrl.data.signosaldofinal == '-' ? 'Saldo pendiente' : 'Saldo a favor'));
    
   return m("div#content-page", [
                 m("div.title-sections[tkey='account-label-account']", m("h3","Estado de cuenta")),
                 m("div.header-edocnta", ctrl.data.apto),
                 m("div.header-edocnta " + clase_saldo, mi_saldo +': '+ctrl.data.saldofinal),
                 m("div.col-xs-12", m("section.ac-container", [
                     (localStorage.getItem("isAdmin") == 'Y'
                         ?
                         m("div", { style: "margin-bottom:20px" }, [m("", "Consultar estado de cuenta por vivienda:"), m("select.form-control", { oninput: m.withAttr("value", ctrl.myApto), value: ctrl.myApto(), onchange: ctrl.changeApto() }, [
                             ctrl.viviendas.map(function (vivienda, indx) {
                                 return m("option", { value: vivienda.Id }, vivienda.Nombre);
                             })
                         ])])
                         :
                         null
                     ),
                     m('div', [
                       m("input", { id: 'ac-2', type: 'checkbox', name: 'accordion-1'}),
                       m("label[for='ac-2'][style='color:#f15927']", "Adeudos pedientes"),
                       m("article.ac-small",
                           ctrl.data.data.adeudos
                           ?
                           [
                           m("table.table table-hover", [
                             m("thead", [
                                 m("tr", [
                                     m("th[style='width:30% !important']", "Fecha"),
                                     m("th[style='width:40% !important']", "Concepto"),
                                     m("th[style='width:20% !important']", "Monto"),
                                     m("th[style='width:10% !important']", "Ver")
                                 ])
                             ]),
                             m("tbody", [
                                 ctrl.data.data.adeudos.map(function (adeudo, indx) {
                                     return m("tr", [
                                                 m("td[style='width:30% !important']", adeudo.fecha),
                                                 m("td[style='width:40% !important']", adeudo.concepto),
                                                 m("td[style='width:20% !important']", adeudo.importe),
                                                 m("td[style='width:10% !important']", [
                                                            m("a.glyphicon glyphicon-eye-open[data-toggle='modal'][data-target='#modal_adeudo_" + indx + "']"),
                                                            m("div.modal fade#modal_adeudo_" + indx + "[role='dialog']", [
                                                                m("div.modal-dialog", [
                                                                    m("div.modal-content", [
                                                                        m("div.modal-header header-adeudo", [
                                                                            m("button.close[data-dismiss='modal']", "X"),
                                                                            m("h4.modal-title", adeudo.concepto)
                                                                        ]),
                                                                        m("div.modal-body", [
                                                                            m("div.col-md-12", [
                                                                                    m("div.row dotted", [
                                                                                        m("div.title-description", "Clasificación:"),
                                                                                        m("div.content-description", adeudo.clasificacion),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Fondo:"),
                                                                                    m("div.content-description", adeudo.fondo),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Descuento:"),
                                                                                    m("div.content-description", adeudo.descuento),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                        m("div.title-description", "Importe:"),
                                                                                    m("div.content-description", adeudo.importe),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                        m("div.title-description", "Fecha:"),
                                                                                    m("div.content-description", adeudo.fecha),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Vencimiento:"),
                                                                                    m("div.content-description", adeudo.vencimiento),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Recargo:"),
                                                                                    m("div.content-description", adeudo.trecargo),
                                                                                    ]),
                                                                                    adeudo.aplicaciones
                                                                                            ?
                                                                                                [
                                                                                                    m("div.modal-aplications", m("strong", "Aplicaciones")),
                                                                                                    m("div.row", [
                                                                                                        m("div.content-description", m("strong", "Fecha")),
                                                                                                        m("div.title-description", m("strong", "Aplicado")),
                                                                                                    ]),
                                                                                                       adeudo.aplicaciones.map(function (app) {
                                                                                                           return [
                                                                                                               m("div.row dotted", [
                                                                                                                   m("div.content-description", app.fecha),
                                                                                                                   m("div.title-description", app.aplicado)
                                                                                                               ]),
                                                                                                           ]
                                                                                                       })
                                                                                                ]
                                                                                            :
                                                                                                ""
                                                                            ])
                                                                        ]),
                                                                        m("div.modal-footer", m("button.btn btn-default[data-dismiss='modal']", "Cerrar"))
                                                                    ])
                                                                ])
                                                            ])
                                                 ])
                                         ])
                                     })
                             ])
                           ])
                       ]
                       :
                       m("div.alert alert-info", "No hay registro de Adeudos pendientes")
                       )
                     ]),
                     m('div', [
                       m("input", { id: 'ac-1', type: 'checkbox', name: 'accordion-1' }),
                       m("label[for='ac-1'][style='color:#8cc63f']", "Resumen de pagos"),
                       m("article.ac-small", 
                           ctrl.data.data.abonos
                           ?
                           [
                           m("table.table table-hover", [
                             m("thead", [
                                 m("tr", [
                                     m("th[style='width:30% !important']", "Fecha"),
                                     m("th[style='width:40% !important']", "Forma de pago"),
                                     m("th[style='width:20% !important']", "Monto"),
                                     m("th[style='width:10% !important']", "Ver")
                                 ])
                             ]),
                             m("tbody", [
                            ctrl.data.data.abonos.map(function (abono, indx) {
                                return m("tr", [
                                                m("td[style='width:30% !important']", abono.fecha),
                                                m("td[style='width:40% !important']", abono.pago),
                                                m("td[style='width:20% !important']", abono.monto),
                                                m("td[style='width:10% !important']", [
                                                            m("a.glyphicon glyphicon-eye-open[data-toggle='modal'][data-target='#modal_abono" + indx + "']"),
                                                            m("div.modal fade#modal_abono" + indx + "[role='dialog']", [
                                                                m("div.modal-dialog", [
                                                                    m("div.modal-content", [
                                                                        m("div.modal-header header-abono", [
                                                                            m("button.close[data-dismiss='modal']", "X"),
                                                                            m("h4.modal-title", ctrl.data.apto + ': ' + abono.naturaleza)
                                                                        ]),
                                                                        m("div.modal-body", [
                                                                            m("div.col-md-12", [
                                                                                    m("div.row dotted", [
                                                                                        m("div.title-description", "Cuenta:"),
                                                                                        m("div.content-description", abono.cuenta),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Fondo:"),
                                                                                    m("div.content-description", abono.fondo),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Referencia:"),
                                                                                    m("div.content-description", abono.referencia),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Pago:"),
                                                                                    m("div.content-description", abono.pago),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Fecha:"),
                                                                                    m("div.content-description", abono.fecha),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Aplicado:"),
                                                                                    m("div.content-description", abono.aplicado),
                                                                                    ]),
                                                                                    m("div.row dotted", [
                                                                                    m("div.title-description", "Comentario:"),
                                                                                    m("div.content-description", abono.comment),
                                                                                    ]),
                                                                                    abono.aplicaciones
                                                                                            ?
                                                                                                [
                                                                                                    m("div.modal-aplications", m("strong","Aplicaciones")),
                                                                                                    m("div.row", [
                                                                                                        m("div.content-description",  m("strong","Concepto")),
                                                                                                        m("div.title-description",  m("strong","Aplicado")),
                                                                                                     ]),
                                                                                                       abono.aplicaciones.map(function (app) {
                                                                                                           return [
                                                                                                               m("div.row dotted", [
                                                                                                                   m("div.content-description", app.concepto),
                                                                                                                   m("div.title-description", app.aplicado)
                                                                                                               ]),
                                                                                                           ]
                                                                                                       })
                                                                                                ]
                                                                                            :
                                                                                                "" 
                                                                            ])
                                                                        ]),
                                                                        m("div.modal-footer", m("button.btn btn-default[data-dismiss='modal']", "Cerrar"))
                                                                    ])
                                                                ])
                                                            ])
                                                ])

                                    ])
                                })
                             ])
                           ])
                       ]
                       :
                       m("div.alert alert-info", "No hay registro de Pagos")
                       )
                     ])
                 ])
                 )
    ])
};