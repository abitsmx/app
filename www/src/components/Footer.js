﻿var Footer = {
    view: function (ctrl) {
        var inSection = localStorage.inSection;
        
        return m("div.footer", [
            m("div", { class: (inSection == "home" ? 'active_home' : 'footer-home') }, [m("a", { href: '/', config: m.route })]),
            m("div", { class: (inSection == "blog" ? 'active_blog' : 'footer-blog') }, [m("a", { href: '/blogs', config: m.route })]),
            m("div", { class: (inSection == "profile" ? 'active_profile' : 'footer-profile') }, [m("a", { href: '/perfil', config: m.route })]),
            m("div", { class: (inSection == "statement" ? 'active_statement' : 'footer-statement') }, [m("a", { href: '/cuenta', config: m.route })]),
            m("div", { class: (inSection == "notifications" ? 'active_notifications' : 'footer-notification') }, [m("a", { href: '/notification', config: m.route })]),
        ]);
    }
};