﻿var url_get_blog = url_base + "Post/";
var url_new_comm = url_base + "Comment/";
var BlogData = {
    controller: function () {
        var ctrl = this;

        ctrl.comment = '';
        ctrl.success = false;
        ctrl.working = true;

        this.comment = m.prop(ctrl.comment || '');

        ctrl.blog = {}
        ctrl.comments = []
        ctrl.images = []
        ctrl.files = []

        var blog = m.request({
            method: "GET",
            url: url_get_blog + m.route.param("blog_id"),
           /* headers: { "APPTOKEN": localStorage.getItem('header') },
            config: function (xhr) {
                xhr.timeout = 10000;
                xhr.ontimeout = function () {
                    alert('Parece que la conexión es un poco lenta.');
                }
            },*/
            background: true,
            initialValue: []
        })
        blog.then(function (response) {
          if(response.status == 'OK') { 
            ctrl.blog.name    = response.data.blog_name;
            ctrl.blog.author = response.data.author;
            ctrl.blog.avatar = response.data.avatar;
            ctrl.blog.nomavatar = response.data.nomavatar;
            ctrl.blog.date    = response.data.date;
            ctrl.blog.comment = response.comment;
            
            if(response.data.images)
              ctrl.images = response.data.images;
            
            if(response.data.files)
              ctrl.files = response.data.files;
            
            ctrl.blog.blog = m.trust(response.data.blog);

            if(response.data.comments)
              ctrl.comments = response.data.comments;

            ctrl.working = false;
          }

          if( response.status == 'ERROR'){
              ctrl.working=false;
          }


        })
        blog.then(m.redraw)

        ctrl.canSubmit = function () {
            return ctrl.comment();
        }

        ctrl.ncomment = function (e) {
            e.preventDefault();

            m.request({
                method: "POST",
                url: url_new_comm + m.route.param("blog_id"),
                headers: { "APPTOKEN": localStorage.getItem('header') },
                config: function (xhr) {
                    xhr.timeout = 10000;
                    xhr.ontimeout = function () {
                        alert('Parece que la conexión es un poco lenta.');
                    }
                },
                data: { comment: ctrl.comment },
                unwrapSuccess: function (response) {
                    if(response.status == 'OK'){

                        if (response.comments)
                          ctrl.comments = response.comments
                      
                      date_post = new Date();
                      
                      ctrl.comments.push({ "avatar": localStorage.getItem('avatar'), "nomavatar":localStorage.getItem("nomavatar"),"author": localStorage.getItem('name'), "comment": ctrl.comment(), "date": date_post });

                      ctrl.comment = m.prop('');
                    
                      ctrl.success = true;
                    }
                    if( response.status == 'ERROR') {
                        alert(response.messages);
                    }
                }
            })
        }
        localStorage.inSection = 'blog';
        if(ctrl.success) m.redraw
    },
    view: function (ctrl) {
        
        if (ctrl.working) {
            return m("div", Working);
        } else {
            son = ctrl.images.length;
            
            if (son == 1 || son == 2 || son == 3)
                col = 4;
            
            if (son > 3)
                col = 3;

            return m("div", [
                m("div.col-xs-3", m("img", { src: ctrl.blog.avatar == "" ? 'images/avatar70x70.png' : ctrl.blog.avatar + '70' + ctrl.blog.nomavatar })),
                m("div.col-xs-9", [
                    m("h3", [
                                m("strong", ctrl.blog.name)
                        ]),
                        m("div.authoring", [
                                                   m("p", [
                                                       m("span", ctrl.blog.author),
                                                       ' - ',
                                                       m("span", ctrl.blog.date),
                                                   ])
                        ]),
                    ]),

                                               m("div.post-content col-md-12", [
                                                   m("div.col-xs-12", [
                                                                   ctrl.images.map(function (img, cnt) {
                                                                       cnt++;
                                                                       return m("div.col-xs-"+col, [
                                                                           m("img.hover-shadow cursor[src='" + img.src + "'][style='width:100%']", { onclick: activeCurrent.bind(this,cnt) })
                                                                       ])
                                                                   })
                                                   ]),
                                                   m("div#myModal.modal", [
                                                       m("span.close btn btn-danger", { onclick: closeModal.bind(this) }, "CERRAR"),
                                                       m("div.modal-content", [
                                                            ctrl.images.map(function (img, cnt) {
                                                               cnt++;
                                                               return m("div.mySlides", m("img[src='" + img.srcNormal + "'][style='width:100%']"))
                                                            }),        
                                                            m("a.prev", { onclick: plusSlides.bind(this,-1) }),
                                                            m("a.next", { onclick: plusSlides.bind(this, 1) }),
                                                       ])
                                                    ]),
                                                    m("div.blog-files", [
                                                        ctrl.files.map(function (file) {
                                                            return m("a",{alt:file.title, href: file.src, target:'_blank'}, m("span.glyphicon glyphicon-paperclip"))
                                                        })
                                                    ]),
                                                   m("div.content-post", ctrl.blog.blog),
                                                   m("div.comments", [
                                                       m("h3", "Comentarios: "),
                                                       (ctrl.blog.comment
                                                   ?
                                                   m("div.labels col-xs-12", [
                                                            (ctrl.success ? m("div.alert alert-success", "Comentario agregado con exito") : ''),
                                                            m("div", [
                                                                m("div.icon", m("img", { src: (localStorage.getItem('avatar') == '' ? 'images/avatar40x40.png' : localStorage.getItem('avatar') + '40' + localStorage.getItem('nomavatar')) })),
                                                                m("div.icomment", [
                                                                    m("textarea#input_comment_add[placeholder='Agregar comentario...'][rows='5']autofocus", { oninput: m.withAttr("value", ctrl.comment), value: ctrl.comment() }),
                                                                    m("button.btn btn-default#btn_add_comment", { onclick: ctrl.ncomment, disabled: !ctrl.canSubmit() }, [
                                                                        m("span.glyphicon glyphicon-send", "")
                                                                    ])
                                                                ])
                                                            ])
                                                   ])
                                                   : m("div.alert alert-info no-comments", "No hay comentarios en este post.")),
                                                       ctrl.comments.map(function (comment) {
                                                           return m("div.comment col-xs-12", [
                                                               m("div.icon", m("img", { src: (comment.avatar == '' ? 'images/avatar40x40.png' : comment.avatar+'40'+comment.nomavatar) })),
                                                               m("div.icomment", [
                                                                    m("div.comment-by", comment.author),
                                                                    m("div.comment-content", m.trust(comment.comment))
                                                               ])
                                                           ])
                                                       })
                                                   ]),
                                                   m("div.separator-footer"),
                                                   m("div.volver", m("a[href='/blogs']", { config: m.route }))
                                               ])
                                    ])

        }
    }
};

function activeCurrent(cnt) {
    openModal();
    currentSlide(cnt);
}
function openModal() {
    document.getElementById('myModal').style.display = "block";
}

function closeModal() {
    document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
}