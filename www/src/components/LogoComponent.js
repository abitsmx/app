﻿var LogoComponent = {
    view: function () {
        if (token)
            return null;

        return m("div.col-md-12", [
                    m("div.the-logo", [
                        m("img[src='images/logo_vivook.svg'][style='width:60%']")
                    ]),
                    m("img.separate-color[src='images/border_header.png']")
        ]);
    }
};