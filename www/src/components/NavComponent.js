﻿var NavComponent = {
    controller:function(){},
    view: function () {

        return m("nav.navbar navbar-inverse", [
            m("div.container-fluid", [
                m("div.navbar-header", [
                    m("button.navbar-toggle", { "data-toggle": "collapse", "data-target": "#myNavbar" }, [
                        m("span.icon-bar"),
                        m("span.icon-bar"),
                        m("span.icon-bar"),
                    ]),
                    m("div.col-md-6[style='width:90% !important']", [
                        m("a.[href='/']", { config: m.route }, m("img.image-header[src='" + localStorage.getItem("logo") + "']")),
                        m("a.navbar-brand image-header-name[href='/perfil']", { config: m.route }, localStorage.getItem('name'))
                    ])
                ]),
                m("div.collapse navbar-collapse#myNavbar", [
                    m("ul.nav navbar-nav", [
                        m("li.active", [
                            m("a[href='/blogs']", { config: m.route }, [
                                m("span", [ m("span.glyphicon glyphicon-list-alt")," Blogs"])
                            ]),
                            m("a[href='/cuenta']", { config: m.route }, [
                                m("span", [ m("span.glyphicon glyphicon-indent-right"), " Estado de cuenta"])
                            ]),
                            m("a[href='/perfil']", { config: m.route }, [m("span", [
                                m("span.glyphicon glyphicon-user")," Perfil" ]) 
                            ]),
                            m("a[href='/help']", { config: m.route }, [m("span", [
                                m("span.glyphicon glyphicon-cog"), " Mejoras"])
                            ]),
                            (localStorage.getItem('isAdmin') == 'Y'
                                ?
                                m("a[href='http://vivook.com/Login.php']", { target: '_blank' }, [m("span", [
                                    m("span.glyphicon glyphicon-briefcase"), " Panel de Administración"])
                                ])
                                :
                                null),
                            m("a[href='/logout']", { config: m.route }, [m("span", [
                                m("span.glyphicon glyphicon-log-out"),
                                " Salir"
                            ])
                            ])
                        ]),
                    ]),
                ]),
            ]),
        ]);
    }
};